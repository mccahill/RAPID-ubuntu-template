#!/bin/bash

#
# We assume that you do a git clone of the repository, cd to the
# directory and run this script from there
#
# where are we being run from?
#
INSTALLFROM=~rapiduser/src

#
# copy the app-specific personalize_me script into place
# this will be run by RAPID when a new instance of the VM is
# cloned from the vmware template 
#
sudo cp $INSTALLFROM/personalize_me.sh /usr/local/bin/personalize_me
sudo chmod ugo+x /usr/local/bin/personalize_me

#
# update the operating system
#
./os-update.sh

#
# personalize this instance
#
sudo /usr/local/bin/personalize_me

sudo reboot

