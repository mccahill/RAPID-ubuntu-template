#!/bin/bash

#
# get to the current version of the OS
#
sudo apt-get update
sudo apt-get dist-upgrade -y --force-yes
sudo apt-get autoremove -y

######################## start unattended upgrades install ######################
#
# update, then install the unattended upgrade package
#
sudo apt-get update
sudo apt-get install -y --force-yes unattended-upgrades update-notifier-common
sudo dpkg-reconfigure -f noninteractive unattended-upgrades

#
# write a settings file into /etc/apt/apt.conf.d/02periodic
#
echo 'APT::Periodic::Enable "1";' >> /tmp/02periodic
echo 'APT::Periodic::BackupArchiveInterval "0";' >> /tmp/02periodic
echo 'APT::Periodic::BackupLevel "0";' >> /tmp/02periodic
echo 'APT::Periodic::Update-Package-Lists "1";' >> /tmp/02periodic
echo 'APT::Periodic::Unattended-Upgrade "1";' >> /tmp/02periodic
sudo cp /tmp/02periodic /etc/apt/apt.conf.d/02periodic
rm /tmp/02periodic


#
# enable unattended reboot via settings file /etc/apt/apt.conf.d/50unattended-upgrades
#
cp /etc/apt/apt.conf.d/50unattended-upgrades /tmp/50unattended-upgrades
echo '   ' >> /tmp/50unattended-upgrades
echo '// enable auto-reboot' >> /tmp/50unattended-upgrades
echo '     Unattended-Upgrade::Automatic-Reboot "true";' >> /tmp/50unattended-upgrades
sudo cp /tmp/50unattended-upgrades /etc/apt/apt.conf.d/50unattended-upgrades 
rm /tmp/50unattended-upgrades


#
# have cron run autoremove once a day to get rid of cruft
#
if [ ! -f /var/spool/cron/crontabs/root ]; then
    sudo cp /dev/null /var/spool/cron/crontabs/root
fi
sudo cp /var/spool/cron/crontabs/root /tmp/root-cron
sudo chmod ugo+w /tmp/root-cron
sudo echo '# automate removing old kernel cruft so we do not fill /boot ' >> /tmp/root-cron
sudo echo '0 8 * * 1 apt-get autoremove -y ' >> /tmp/root-cron
sudo echo ' ' >> /tmp/root-cron
sudo cp /tmp/root-cron /var/spool/cron/crontabs/root
sudo chmod go-w /var/spool/cron/crontabs/root
sudo rm /tmp/root-cron


#
# to run the automatic upgrade process manually (and verify that it works)
#        sudo /usr/bin/unattended-upgrade
# then check the log file(s) found at
#        ls -l /var/log/unattended-upgrades/*
#
# btw, the crontask that invokes the upgrades is
#        /etc/cron.daily/apt
#
# and more background reading can be found here:
#        http://askubuntu.com/questions/524692/unattended-upgrades-mail-only-on-error-or-reboot
#        http://askubuntu.com/questions/656041/which-unattended-upgrades-system-takes-precedence-and-how/656116#656116
#
#
######################## end unattended upgrades install ######################


#
# add the VMware Open VM Tools
#
mkdir /tmp/vmware-keys
cd /tmp/vmware-keys
wget https://packages.vmware.com/tools/keys/VMWARE-PACKAGING-GPG-DSA-KEY.pub
wget https://packages.vmware.com/tools/keys/VMWARE-PACKAGING-GPG-RSA-KEY.pub
sudo apt-key add /tmp/vmware-keys/VMWARE-PACKAGING-GPG-DSA-KEY.pub
sudo apt-key add /tmp/vmware-keys/VMWARE-PACKAGING-GPG-RSA-KEY.pub
echo 'deb http://packages.vmware.com/packages/ubuntu trusty main' > /tmp/vmware-keys/vmware-tools.list
sudo cp /tmp/vmware-keys/vmware-tools.list /etc/apt/sources.list.d/vmware-tools.list
sudo apt-get update
sudo apt-get install open-vm-tools-deploypkg -y
rm -rf /tmp/vmware-keys


#
# NOTE: right before making an Ubuntu image into a VMware template
# we need to make sure that the current DHCP lease is removed. You
# can do this with these commands:
#
# sudo dhclient -r -v eth0 ; sudo rm /var/lib/dhcp/dhclient.* 
#


