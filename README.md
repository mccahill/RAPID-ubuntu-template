To build the template, you need to run two scripts
and the app-install script will do that for you.

The os-update.sh script is used to make sure that an
Ubuntu 14.04 linux is up to date, and will automatically 
patch itself. You probably want this ;-)

The personalize_me.sh script is intended to be run on
instances created from the template - this is where we
change the server name here to match its entry in the DNS. 
This install puts the script into the place where the
RAPID provisioning engine will run it: 

     /usr/local/bin/personalize_me	 

How to install

1.) Use git to grab a copy of the source

     git clone http://gitlab.oit.duke.edu/mccahill/RAPID-ubuntu-template.git

2.) From an account with sudo rights, rename the directory you just cloned,
    then go to the directory:

     mv RAPID-ubuntu-template ~rapiduser/src
	  cd ~rapiduser/src

3.) Make sure the OS is updated and the personalize_me script is isntalled. 
    A script to do this for Ubuntu 14.04 is part of the git repo. Note that 
	this script will reboot the VM.
   
	./app-install.sh



