#!/bin/bash
#
# fix the hostname
#
MYIP=`ifconfig eth0 | grep "inet addr" | cut -d ':' -f2 | cut -d ' ' -f1`
RAWNAME=`nslookup $MYIP | grep "name =" | cut -d '=' -f2 | cut -d ' ' -f2`
REALNAME=${RAWNAME:0:`expr length $RAWNAME - 1`}
SHORTNAME=`echo $RAWNAME | cut -d '.' -f1`
sudo hostname $SHORTNAME
rm -f /tmp/hosts
echo '127.0.0.1 ' ' localhost' >> /tmp/hosts
echo '127.0.1.1 ' $REALNAME     ' ' $SHORTNAME >> /tmp/hosts
echo ' ' >> /tmp/hosts
echo '# The following lines are desirable for IPv6 capable hosts' >> /tmp/hosts
echo '::1     localhost ip6-localhost ip6-loopback' >> /tmp/hosts
echo 'ff02::1 ip6-allnodes' >> /tmp/hosts
echo 'ff02::2 ip6-allrouters' >> /tmp/hosts
sudo cp /tmp/hosts /etc/hosts
rm -f /tmp/hostname
echo $SHORTNAME >> /tmp/hostname
sudo cp /tmp/hostname /etc/hostname
rm /tmp/hostname
rm /tmp/hosts
sudo hostnamectl set-hostname $SHORTNAME


#
# return an exit code
#
exit 0


